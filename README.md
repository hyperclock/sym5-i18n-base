Sym5 i18n Base
==============

is short for _**Symfony 5 International Base**_, which will be my starting point for symfony based projects. I thought this may be usefull for others having problems getting a setup like this.

This project is started with _Symfony 5.1.7_ (--full).


## Documentation

will be up as soon as I have something to document. Just check the [wiki](../../wikis) every now and then.


## License
BSD 3-Clause "New" or "Revised" License, see [LICENSE](LICENSE) for details.

A permissive license similar to the BSD 2-Clause License, but with a 3rd clause that prohibits others from using the name of the project or its contributors to promote derived products without written consent.

**Permissions**
* Commercial use
* Modification
* Distribution
* Private use

**Limitations**
* No Liability
* No Warranty

**Conditions**
* License and copyright notice must be included with the software.
